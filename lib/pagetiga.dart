import 'package:flutter/material.dart';

class PageTiga extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(centerTitle: true, title: Text("Detail Biodata")),
        body: Container(
          padding: EdgeInsets.all(15.0),
          child: Column(
            children: <Widget>[
              Text(
                "Nama Lengkap : Riza Riswanto",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text("TTL : Bandung, 27 Maret 1981 "),
              Text("Hobi : Bulutangkis"),
              Text("Alamat : Bandung"),
              Image.asset("assets/Rzaholic.jpg"),
            ],
          ),
        ));
  }
}
