import 'package:flutter/material.dart';
import 'package:tugas_mobpro2/loginpage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Biodata",
      home: LoginPage(),
    );
  }
}
