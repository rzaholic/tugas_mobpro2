import 'package:flutter/material.dart';
import 'package:tugas_mobpro2/pagedua.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController nama = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.lime,
      appBar: AppBar(
        title: Text("Halaman Login"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Center(
          child: Column(children: <Widget>[
            TextField(
              controller: nama,
              decoration: new InputDecoration(
                hintText: "Nama User",
                labelText: "Nama",
                icon: Icon(Icons.people),
                border: OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                ),
              ),
              // keyboardType: TextInputType.numberWithOptions(),
            ),
            RaisedButton(
              onPressed: () {
                var namasaya = nama.text;
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (_) {
                      var pageDua = PageDua(
                        namakamu: namasaya.toString(),
                      );
                      return pageDua;
                    },
                  ),
                );
              },
              child: Text("Login"),
              color: Colors.grey,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
            ),
          ]),
        ),
      ),
    );
  }
}
