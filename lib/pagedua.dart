import 'package:flutter/material.dart';
import 'package:tugas_mobpro2/pagetiga.dart';

class PageDua extends StatelessWidget {
  final namakamu;
  PageDua({@required this.namakamu});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(centerTitle: true, title: Text("Biodata")),
        body: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Selamat Datang $namakamu",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Image.asset("assets/Rzaholic.jpg"),
              RaisedButton(
                  child: Text('Detail'),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return PageTiga();
                    }));
                  })
            ],
          ),
        ));
  }
}
